import {useSelector} from 'react-redux'
const Home = () => {
   //const {user} = useSelector((state) => ({...state}))
    const user = useSelector((state) => state.user);
        return<div className="container-fluid h1 p5 text-center">
            Home Page
            {JSON.stringify(user)}
        </div>
};


//OR return<div className="container-fluid h1 p5 text-center">
//Home Page
//  <p>Name: {user.name}</p>
//  <p>Role: {user.role}</p>
//</div>


export default Home;