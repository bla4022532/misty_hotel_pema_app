import {useState} from "react";
import RegisterForm from "../components/RegisterForm";
import axios from 'axios';

const Register = () => {
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");


    const handleSubmit = async (e) => {
        e.preventDefault();
       // alert("send user data to backend")
       //console.table({name, email, password});
        try{
            const res = await axios.post(`http://localhost:8000/register`,{
            name,
            email,
            password,
        });
        console.log('Register User ==>', res);
        }catch (err){
            console.log(err);
        }
    };

    return(
        <>
            <div className="container-fluid bg-info p-5 text-center">
                <h1>Register Page</h1>
            </div>
            <div className="container">
                <div className="row">
                    <div className="col-md-6 offset-md-3">
                        <RegisterForm
                            handleSubmit={handleSubmit}
                            name={name}
                            setName={setName}
                            email={email}
                            setEmail={setEmail}
                            password={password}
                            setPassword={setPassword}
                        />
                    </div>
                </div>
            </div>
        </>
    );
};
export default Register;