//create user reducer function
export const authReducer = (state = { name: "Kinley", role:"Seller"}, action) => {
    switch(action.type){
      case "LOGGED_IN":
        return {...state, ...action.payload}
      case 'LOGOUT':
        return action.payload;
      default:
        return state;
    }
  };
