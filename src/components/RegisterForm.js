const registerForm = ({
    handleSubmit,
    name,
    setName,
    email,
    setEmail,
    password,
    setPassword,
}) => (
    <form onSubmit={handleSubmit}>
        <div className="form-group">
            <input
                type="text"
                className="form-control m-3"
                placeholder="Enter Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
            />
            <input
                type="email"
                className="form-control m-3"
                placeholder="Enter Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
            />
            <input
                type="password"
                className="form-control m-3"
                placeholder="Enter Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
            />
        </div>
    <button type="submit" className="btn btn-primary m-3">Submit</button>
    </form>
)
export default registerForm;