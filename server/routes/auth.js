import express from 'express'


const router = express.Router()


//import controllers
import { showMessage, register } from '../controllers/auth';
/*router.get("/:message", (req, res) => {
    res.status(200).send(req.params.message);
});*/


router.get("/:message", showMessage);
router.post('/register', register);


// Export the router so that it can be used in other parts of your application
//export default router;
module.exports = router;